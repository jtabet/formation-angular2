import { Directive, ElementRef } from '@angular/core';
@Directive({ selector: '[redify]' })
export class Redify {
    constructor(el: ElementRef) {
       el.nativeElement.style.color = 'red';
    }
}