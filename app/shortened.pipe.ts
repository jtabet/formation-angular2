import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'shortened'})
export class Shortened implements PipeTransform {
  transform(value: string): string {
    return value.slice(-2);
  }
}
