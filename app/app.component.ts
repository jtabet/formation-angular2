import { Component } from '@angular/core';
import { Redify } from './redify.directive';
import { Shortened } from './shortened.pipe';

@Component({
    selector: 'my-app',
    directives: [Redify],
    pipes: [Shortened],
    templateUrl: 'build/templates/default.html',
})
export class AppComponent {
    clicked: boolean;
    clickedElement: string;
    inputContent: string;

    names: string[] = [
        'Doni',
        'Clément',
        'Jérémie'
    ];

    onClick(clickedElement :string){
        this.clickedElement = clickedElement;
        this.clicked = true;
    }

    onClear(){
        this.clickedElement = '';
        this.clicked = false;
    }

    isCurrent(name :string):boolean{
        return name === this.clickedElement;
    }
}
